#include "pch.h"
#include "Texture.h"


Texture::Texture(const char* filename, GLenum type, GLint texture_unit)
{
	if (this->id) {
		glDeleteTextures(1, &this->id);
	}
	this->type = type;
	this->textureUnit = texture_unit;

	// Load image with SOIL
	unsigned char* image = SOIL_load_image(filename, &this->width, &this->height, NULL, SOIL_LOAD_RGBA);

	// Make and bind texture
	glGenTextures(1, &this->id);
	glBindTexture(type, this->id);

	// Set parameters
	glTexParameteri(type, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(type, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(type, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (image) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width, this->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(type);
	}
	else {
		cout << "Texture loading failed: " << filename << endl;
	}

	glActiveTexture(0);
	glBindTexture(type, 0);
	SOIL_free_image_data(image);
}


Texture::~Texture()
{
	glDeleteTextures(1, &this->id);
}

GLuint Texture::getID() const
{
	return this->id;
}

void Texture::bind()
{
	glActiveTexture(GL_TEXTURE0 + this->textureUnit);
	glBindTexture(this->type, this->id);
}

void Texture::unbind()
{
	glActiveTexture(0);
	glBindTexture(this->type, 0);
}

GLuint Texture::getTextureUnit() const
{
	return this->textureUnit;
}

void Texture::loadFromFile(const char * fileName)
{
	if (this->id) {
		glDeleteTextures(1, &this->id);
	}

	// Load image with SOIL
	unsigned char* image = SOIL_load_image(fileName, &this->width, &this->height, NULL, SOIL_LOAD_RGBA);

	// Make and bind texture
	glGenTextures(1, &this->id);
	glBindTexture(this->type, this->id);

	// Set parameters
	glTexParameteri(this->type, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(this->type, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(this->type, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(this->type, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (image) {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, this->width, this->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
		glGenerateMipmap(this->type);
	}
	else {
		cout << "Texture loading failed: " << fileName << endl;
	}

	glActiveTexture(0);
	glBindTexture(this->type, 0);
	SOIL_free_image_data(image);
}
