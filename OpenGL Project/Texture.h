#pragma once

#include <iostream>

#include <GL/glew.h>
#include <GL/freeglut.h>

#include<SOIL2.h>

#include "glsl.h"

class Texture
{
private:
	GLuint id;
	int width;
	int height;
	unsigned int type;
	GLuint textureUnit;
public:
	Texture(const char* filename, GLenum type, GLint texture_unit);
	~Texture();
	GLuint getID() const;
	void bind();
	void unbind();
	GLuint getTextureUnit() const;
	void loadFromFile(const char* fileName);
};

