// OpenGL Project.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "libs.h"



//--------------------------------------------------------------------------------
// Consts
//--------------------------------------------------------------------------------

const int WIDTH = 960, HEIGHT = 540;
unsigned const int DELTA = 10;
const char * fragshader_name = "fragmentshader.fsh";
const char * vertexshader_name = "vertexshader.vsh";

//--------------------------------------------------------------------------------
// Variables
//--------------------------------------------------------------------------------
Shader *program;
Texture *texture0, *texture1;
Material *material0;
Mesh *mesh;

// Matrices
glm::mat4 ViewMatrix(1.f);
glm::mat4 ProjectionMatrix(1.f);

glm::vec3 camPosition(0.f, 0.f, 1.f);


//--------------------------------------------------------------------------------
// Keyboard handling
//--------------------------------------------------------------------------------

void keyboardHandler(unsigned char key, int a, int b)
{
	// ESCAPE
	if (key == 27) {
		glutExit();
	}
	// W
	if (key == 119) {
		mesh->move(glm::vec3(0.f, 0.f, 0.01f));
	}
	// A
	if (key == 97) {
		mesh->move(glm::vec3(-0.01f, 0.f, 0.f));
	}
	// S
	if (key == 115) {
		mesh->move(glm::vec3(0.f, 0.f, -0.01f));
	}
	// D
	if (key == 100) {
		mesh->move(glm::vec3(0.01f, 0.f, 0.f));
	}
	// Q == ROTATE Y
	if (key == 113) {
		mesh->rotate(glm::vec3(0.f, -1.f, 0.f));
	}
	// E == ROTATE Y
	if (key == 101) {
		mesh->rotate(glm::vec3(0.f, 1.f, 0.f));
	}
	// Z == SCALE UP
	if (key == 122) {
		mesh->scaleMesh(glm::vec3(0.1f));
	}
	// X == SCALE DOWN
	if (key == 120) {
		mesh->scaleMesh(glm::vec3(-0.1f));
	}
}

//--------------------------------------------------------------------------------
// Rendering
//--------------------------------------------------------------------------------

void Render()
{
	glClearColor(0.f, 0.f, .5f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	program->useShader();

	// Lights
	glm::vec3 lightPos0 = glm::vec3(0.f, 0.f, 1.f);

	// Update uniforms
	program->set1i(texture0->getTextureUnit(), "texture0");
	program->set1i(texture1->getTextureUnit(), "texture1");

	material0->sendToShader(*program);

	program->setMat4fv(ViewMatrix, "ViewMatrix");
	program->setMat4fv(ProjectionMatrix, "ProjectionMatrix");

	program->setVec3f(lightPos0, "lightPos0");
	program->setVec3f(camPosition, "cameraPos");

	program->useShader();

	// Activate textures
	texture0->bind();
	texture1->bind();

	mesh->render(program);

	// End Draw
	glutSwapBuffers();

	// Unbind everything
	glBindVertexArray(0);
	glUseProgram(0);
	glActiveTexture(0);
	glBindTexture(GL_TEXTURE_2D, 0);
}



//------------------------------------------------------------
// void Render(int n)
// Render method that is called by the timer function
//------------------------------------------------------------

void Render(int n)
{
	Render();
	glutTimerFunc(DELTA, Render, 0);
}


//------------------------------------------------------------
// void InitTextures()
// Initializes textures
//------------------------------------------------------------

void InitTextures() 
{
	texture0 = new Texture("Images/brick.png", GL_TEXTURE_2D, 0);
	texture1 = new Texture("Images/brick2.png", GL_TEXTURE_2D, 1);

	material0 = new Material(glm::vec3(0.1f), glm::vec3(1.f), glm::vec3(1.f), texture0->getTextureUnit(), texture1->getTextureUnit());
}


//------------------------------------------------------------
// void InitMatrices()
// Initializes matrices
//------------------------------------------------------------

void InitMatrices()
{
	glm::vec3 worldUp = glm::vec3(0.f, 1.f, 0.f);
	glm::vec3 camFront = glm::vec3(0.f, 0.f, -1.f);

	ViewMatrix = glm::lookAt(camPosition, camPosition + camFront, worldUp);

	float fov = 90.f;
	float nearPlane = 0.1f;
	float farPlane = 1000.f;

	ProjectionMatrix = glm::perspective(
		glm::radians(fov), 
		1.0f * WIDTH / HEIGHT, 
		nearPlane, 
		farPlane);
}

//------------------------------------------------------------
// void InitGlutGlew(int argc, char **argv)
// Initializes Glut and Glew
//------------------------------------------------------------

void InitGlutGlew(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(WIDTH, HEIGHT);
	glutCreateWindow("OpenGL World");
	glutDisplayFunc(Render);
	glutKeyboardFunc(keyboardHandler);
	glutTimerFunc(DELTA, Render, 0);

	glewInit();
}



int main(int argc, char ** argv)
{
	InitGlutGlew(argc, argv);
	program = new Shader(vertexshader_name, fragshader_name);
	mesh = new Mesh(&Quad());
	InitTextures();
	InitMatrices();

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);

	glutMainLoop();

	return 0;
}
