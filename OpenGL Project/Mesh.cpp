#include "pch.h"
#include "Mesh.h"

void Mesh::initVAO(Primitive * primitive)
{
	// Set variables
	this->nrOfVertices = primitive->getNrOfVertices();
	this->nrOfIndices = primitive->getNrOfIndices();

	// Create vao
	glCreateVertexArrays(1, &this->vao);
	glBindVertexArray(this->vao);

	// Create vbo
	glGenBuffers(1, &this->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glBufferData(GL_ARRAY_BUFFER, this->nrOfVertices * sizeof(Vertex), primitive->getVertices(), GL_STATIC_DRAW);

	// Create ebo
	glGenBuffers(1, &this->ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->nrOfIndices * sizeof(GLint), primitive->getIndices(), GL_STATIC_DRAW);

	// Position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
	glEnableVertexAttribArray(0);

	// Color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, color));
	glEnableVertexAttribArray(1);

	// Texcoord
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoord));
	glEnableVertexAttribArray(2);

	// Normal
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);
}

void Mesh::initVAO(Vertex* vertexArray,
	const unsigned& nrOfVertices,
	GLuint* indexArray,
	const unsigned& nrOfIndices)
{
	// Set variables
	this->nrOfVertices = nrOfVertices;
	this->nrOfIndices = nrOfIndices;

	// Create vao
	glCreateVertexArrays(1, &this->vao);
	glBindVertexArray(this->vao);

	// Create vbo
	glGenBuffers(1, &this->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
	glBufferData(GL_ARRAY_BUFFER, this->nrOfVertices * sizeof(Vertex), vertexArray, GL_STATIC_DRAW);

	// Create ebo
	glGenBuffers(1, &this->ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->nrOfIndices * sizeof(GLint), indexArray, GL_STATIC_DRAW);

	// Position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, position));
	glEnableVertexAttribArray(0);

	// Color
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, color));
	glEnableVertexAttribArray(1);

	// Texcoord
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texcoord));
	glEnableVertexAttribArray(2);

	// Normal
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));
	glEnableVertexAttribArray(3);

	glBindVertexArray(0);
}

void Mesh::updateUniforms(Shader * shader)
{
	shader->setMat4fv(this->ModelMatrix, "ModelMatrix");
}

void Mesh::updateModelMatrix()
{
	// Reset matrix
	this->ModelMatrix = glm::mat4(1.f);
	// Translation
	this->ModelMatrix = glm::translate(this->ModelMatrix, this->position);
	// Rotate X
	this->ModelMatrix = glm::rotate(this->ModelMatrix, glm::radians(this->rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
	// Rotate Y
	this->ModelMatrix = glm::rotate(this->ModelMatrix, glm::radians(this->rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
	// Rotate Z
	this->ModelMatrix = glm::rotate(this->ModelMatrix, glm::radians(this->rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
	// Scale
	this->ModelMatrix = glm::scale(this->ModelMatrix, this->scale);
}

Mesh::Mesh(Vertex* vertexArray,	const unsigned& nrOfVertices,	GLuint* indexArray,	const unsigned& nrOfIndices,
	glm::vec3 position,
	glm::vec3 rotation,
	glm::vec3 scale)
{
	this->position = position;
	this->rotation = rotation;
	this->scale = scale;
	this->initVAO(vertexArray, nrOfVertices, indexArray, nrOfIndices);
	this->updateModelMatrix();

}

Mesh::Mesh(Primitive * primitive, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
{
	this->position = position;
	this->rotation = rotation;
	this->scale = scale;
	this->initVAO(primitive);
	this->updateModelMatrix();
}


Mesh::~Mesh()
{
	glDeleteVertexArrays(1, &this->vao);
	glDeleteBuffers(1, &this->vbo);
	glDeleteBuffers(1, &this->ebo);
}

void Mesh::setPosition(const glm::vec3 position)
{
	this->position = position;
}

void Mesh::setRotation(const glm::vec3 rotation)
{
	this->rotation = rotation;
}

void Mesh::setScale(const glm::vec3 scale)
{
	this->scale = scale;
}

void Mesh::move(const glm::vec3 position)
{
	this->position += position;
}

void Mesh::rotate(const glm::vec3 rotation)
{
	this->rotation += rotation;
}

void Mesh::scaleMesh(const glm::vec3 scale)
{
	this->scale += scale;
}

void Mesh::update()
{

}

void Mesh::render(Shader * shader)
{
	this->updateModelMatrix();
	this->updateUniforms(shader);

	shader->useShader();
	// Bind vertex array object
	glBindVertexArray(this->vao);

	// Draw
	glDrawElements(GL_TRIANGLES, this->nrOfIndices, GL_UNSIGNED_INT, 0);


	shader->unuseShader();
}
